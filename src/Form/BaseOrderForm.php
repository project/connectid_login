<?php

namespace Drupal\connectid_login\Form;

use ConnectId\Api\DataModel\Address;
use ConnectId\Api\DataModel\CouponType;
use ConnectId\Api\DataModel\CouponTypeList;
use ConnectId\Api\DataModel\Order;
use ConnectId\Api\DataModel\OrderLine;
use ConnectId\Api\DataModel\ProductType;
use ConnectId\Api\DataModel\ProductTypeList;
use ConnectId\OAuth2\Client\Provider\Exception\InvalidAccessTokenException;
use DateTimeImmutable;
use Drupal\connectid\ClientApiServiceInterface;
use Drupal\connectid\UserApiServiceInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use InvalidArgumentException;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a ConnectID API Access form.
 */
abstract class BaseOrderForm extends FormBase {

  /**
   * ConnectID Client APi.
   *
   * @var \Drupal\connectid\ClientApiServiceInterface
   */
  protected ClientApiServiceInterface $connectIdClientApi;

  /**
   * ConnectID User/Logged-in API
   * @var \Drupal\connectid\UserApiServiceInterface
   */
  protected UserApiServiceInterface $connectIdUserApi;

  /**
   * Simple constructor.
   */
  public function __construct(
    ClientApiServiceInterface $client_api_service,
    UserApiServiceInterface $user_api_service
  ) {
    $this->connectIdClientApi = $client_api_service;
    $this->connectIdUserApi = $user_api_service;
  }

  /**
   * Service injector constructor.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('connectid.client_api'),
      $container->get('connectid.user_api')
    );
  }

  /**
   * Submission handler for coupon selection.
   */
  public function ajaxCouponSelectionElement(
    array &$form,
    FormStateInterface $form_state
  ): array {
    return $form['product']['coupon_id'];
  }

  /**
   * Builds a Product + Coupon selection.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   Form API elements to select Product and Coupon.
   */
  protected function getProductSelectionElement(
    FormStateInterface $form_state
  ): array {
    $form['product'] = [
      '#type'  => 'fieldset',
      '#label' => $this->t('Product'),
    ];

    $product_options = [];
    foreach ($this->getAvailableProducts() as $product) {
      assert($product instanceof ProductType);
      $product_options[$product->getProduct()] = $product->getDescription();
    }
    $form['product']['product_id'] = [
      '#type'          => 'select',
      '#title'         => t('Product'),
      '#options'       => $product_options,
      '#default_value' => $form_state->getValue('product_id'),
      '#required'      => TRUE,
      '#ajax'          => [
        'callback'        => '::ajaxCouponSelectionElement',
        'disable-refocus' => TRUE,
        'event'           => 'change',
        'wrapper'         => 'coupon-wrapper',
        'progress'        => [
          'type'    => 'throbber',
          'message' => $this->t('Loading coupons...'),
        ],
      ],
    ];

    $form['product']['coupon_id'] = [
      '#type'          => 'select',
      '#title'         => t('Coupon'),
      '#options'       => [],
      '#default_value' => $form_state->getValue('coupon_id'),
      '#prefix'        => '<div id="coupon-wrapper">',
      '#suffix'        => '</div>',
      '#required'      => TRUE,
    ];

    if ($productId = $form_state->getValue('product_id')) {
      assert(is_string($productId));
      $coupon_options = [];
      foreach ($this->getAvailableCoupons($productId) as $coupon) {
        assert($coupon instanceof CouponType);
        $coupon_options[$coupon->getId()] = $coupon->getDescription();
      }

      $form['product']['coupon_id']['#options'] = $coupon_options;
    }
    else {
      $form['product']['coupon_id']['#empty_option'] = $this->t(
        '- Select a product to enable-'
      );
      $form['product']['coupon_id']['#disabled'] = TRUE;
    }


    return $form['product'];
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param string                               $title
   *   Fieldset title
   *
   * @return array
   */
  protected function getAddressFieldset(
    FormStateInterface $form_state,
    string $title = 'Address'
  ): array {
    $fieldset = [
      '#type'  => 'fieldset',
      '#title' => t($title),
      '#tree'  => TRUE,
    ];

    $fieldset['recipient_type'] = [
      '#type'    => 'radios',
      '#title'   => t('Recipient type'),
      '#options' => [
        'company' => t('Company'),
        'private' => t('Private'),
      ],
    ];

    $fieldset['companyName'] = [
      '#type'        => 'textfield',
      '#title'       => t('Company name'),
      '#placeholder' => t('Company name'),
    ];

    $fieldset['firstName'] = [
      '#type'        => 'textfield',
      '#title'       => t('First name'),
      '#placeholder' => t('First name'),
    ];

    $fieldset['lastName'] = [
      '#type'        => 'textfield',
      '#title'       => t('Last name'),
      '#placeholder' => t('Last name'),
    ];

    $fieldset['street'] = [
      '#type'        => 'textfield',
      '#title'       => t('Address'),
      '#placeholder' => t('Address'),
    ];

    $fieldset['postalCode'] = [
      '#type'        => 'textfield',
      '#title'       => t('Postal/ZIP code'),
      '#placeholder' => t('Postal/ZIP code'),
    ];

    $fieldset['postalPlace'] = [
      '#type'        => 'textfield',
      '#title'       => t('City/Area'),
      '#placeholder' => t('City/Area'),
    ];

    $fieldset['phone'] = [
      '#type'        => 'textfield',
      '#title'       => t('Telephone number'),
      '#placeholder' => t('Telephone number'),
      '#required'    => TRUE
    ];

    $fieldset['email'] = [
      '#type'        => 'textfield',
      '#title'       => t('Email address'),
      '#placeholder' => t('Email address'),
      '#required'    => TRUE
    ];

    return $fieldset;
  }

  protected function getPaymentMethodElement(
    FormStateInterface $form_state
  ): array {
    // TODO: Make this configurable
    $payment_methods = [
      Order::PAYMENT_TYPE_CARD_NETS => t('Credit Card'),
      Order::PAYMENT_TYPE_INVOICE => t('Invoice')
    ];

    return [
      '#type'     => 'radios',
      '#title'    => t('Payment method'),
      '#options'  => $payment_methods,
      '#required' => TRUE,
    ];
  }


  /**
   * Checks if we have a valid return Node ID set.
   *
   * This comes in the form of `return_node` query parameter.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|null
   *   Form element or nothing.
   */
  protected function getSourceNodeElement(FormStateInterface $form_state): ?array {
    $query = $this->getRequest()->query;
    if ($query->has('return_node')) {
      $nid = $query->getInt('return_node');
      try {
        // Try to create a URL to the given node, if this fails we don't have  a valid return route.
        Url::fromRoute('entity.node.canonical', ['node' => $nid]);
        return [
          '#type' => 'value',
          '#value' => $nid,
        ];

      }
      catch (\Exception $e) {
        // Do nothing, consider it invalid.
      }
    }

    return NULL;
  }

  protected function createOrderFromSubmission(FormStateInterface $form_state): Order {
    $order_extra_data = [];
    if ($form_state->hasValue('source_node_id')) {
      $order_extra_data['sourceNodeId'] = $form_state->getValue('source_node_id');
    }

    $order = Order::create($order_extra_data);
    $requestTime = DateTimeImmutable::createFromFormat('U', \Drupal::time()->getRequestTime());
    $order->withOrderDate($requestTime)
      ->withPaymentMethod($form_state->getValue('paymentMethod'))
      ->withCurrency('NOK');

    $billing_address = $this->createConnectIdAddressFromInput(
      $form_state->getValue('billing_address')
    );
    $order->withPayer($billing_address);

    $coupon = $this->getCouponObject($form_state->getValue('coupon_id'));
    $order->withOrderAmount($coupon->getCouponPrice());
    $order_line = $this->getOrderLineForCoupon($coupon);
    return $order->withOrderLine($order_line);
  }

  /**
   * Creates an Address object from an array of submitted data.
   *
   * The data in the array is expected to reflect a fieldset as generated
   *  by BaseOrderForm::getAddressFieldset()
   *
   * @see self::getAddressFieldset()
   *
   * @param array $form_values
   *   The array of data.
   *
   * @return \ConnectId\Api\DataModel\Address
   *   The Address object ready to use.
   */
  protected function createConnectIdAddressFromInput(array $form_values): Address {
    if (!empty($form_values['email'])) {
      $form_values['emails'] = [
        $form_values['email'],
      ];
      unset($form_values['email']);
    }

    if (!empty($form_values['phone'])) {
      // Only keep numbers and + (for int. prefix)
      $phone = preg_replace("/[^+\d]/", '', $form_values['phone']);
      // Check if it's the standard Norwegian 8 char mobile number
      if (strlen($phone) === 8) {
        $phone = '+47' . $phone;
      }

      $form_values['phoneNumbers'] = [$phone];
      unset($form_values['phone']);
    }

    unset($form_values['recipient_type']);

    return Address::create($form_values);
  }

  /**
   * Submits an order to the API.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \ConnectId\Api\DataModel\Order       $order
   *
   * @return \ConnectId\Api\DataModel\Order
   *   The submitted order which includes an Order identifier.
   */
  protected function submitOrder(
    FormStateInterface $form_state,
    Order $order
  ): ?Order {
    if (!$this->isUserConnected()) {
      throw new \RuntimeException(
        $this->t('Cannot submit an order for current user: missing external authentication with ConnectID.')
      );
    }

    /** @var \Drupal\openid_connect\OpenIDConnectSessionInterface $oidc_session */
    $oidc_session = \Drupal::service('openid_connect.session');
    $token = $oidc_session->retrieveAccessToken();

    try {
      $submitted_order = $this->connectIdUserApi->registerOrder(
        new AccessToken(['access_token' => $token]),
        $order
      );
    }
    catch (InvalidAccessTokenException $exception) {
      // Expired token, we need to re-authenticate the user.
      $this->storeOrderAndConnectUser($form_state, $order);
      return NULL;
    }

    return $submitted_order;
  }

  protected function isUserConnected(): bool {
    // Anonymous cannot be a connected user.
    if ($this->currentUser()->isAnonymous()) {
      return FALSE;
    }

    /** @var \Drupal\externalauth\AuthmapInterface $svc */
    $svc = \Drupal::service('externalauth.authmap');
    $sub = $svc->get($this->currentUser()->id(), 'openid_connect.connectid');
    // If we cannot load the authmap, then we are sure that the user is not connected.
    if ($sub === FALSE) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Store an order in a temporary storage and attempt logging in the user cia ConnectID.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param \ConnectId\Api\DataModel\Order $order
   *   Order to submit.
   */
  protected function storeOrderAndConnectUser(FormStateInterface $form_state, Order $order): void {
    // Ensure user is logged out as we will need to re-authenticate to prevent
    // the usage of expired access tokens.
    if ($this->currentUser()->isAuthenticated()) {
      user_logout();
    }

    // Store the order using a unique UUID which will be passed as parameter.
    /** @var \Drupal\connectid_login\OrdersHelperInterface $helper */
    $helper = \Drupal::service('connectid_login.orders_helper');
    $order_uuid = $helper->temporaryStoreOrder($order);

    $submit_order_url = Url::fromRoute('connectid_login.order_submission', ['order_uuid' => $order_uuid]);
    $form_state->setResponse(
      $this->redirect('openid_connect.login', [], ['query' => ['destination' => $submit_order_url->toString()]])
    );
  }


  /**
   * This sets the form redirect to the final "order fulfillment" page.
   *
   * This page is hosted on ConnectID and therefore requires us to create a
   *  TrustedRedirectResponse ad-hoc to allow the redirect.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \ConnectId\Api\DataModel\Order       $order
   */
  protected function setOrderFulfillRedirect(
    FormStateInterface $form_state,
    Order $order
  ): void {
    // Magic data set in the order process ot handle the redirect.
    if ($form_state->hasValue('source_node_id')) {
      $return_url = Url::fromRoute('entity.node.canonical', ['node' => $form_state->getValue('source_node_id')]);
    }
    else {
      // Default to front if we're missing this info.
      $return_url = Url::fromRoute('<front>');
    }
    $return_url = $return_url->setAbsolute()->toString(TRUE);
    $redirect = new RedirectResponse(
      $this->connectIdUserApi->getOrderFulfillmentUrl($order, $return_url->getGeneratedUrl())->getUri()
    );

    $form_state->setResponse(TrustedRedirectResponse::createFromRedirectResponse($redirect));
  }

  /**
   * Loads a coupon from the API given an identifier.
   *
   * @param string $coupon
   *
   * @return \ConnectId\Api\DataModel\CouponType
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *  If the coupon is not found.
   *
   */
  protected function getCouponObject(string $coupon): CouponType {
    $couponObject = $this->connectIdClientApi->getCoupon($coupon);
    if (!$couponObject) {
      throw new InvalidArgumentException('Could not load the coupon: ' . $coupon);
    }

    return $couponObject;
  }

  /**
   * Creates an "order line" for a given product/coupon.
   *
   * @param \ConnectId\Api\DataModel\CouponType $coupon
   *   Coupon to include in the order line.
   * @param int $quantity
   *   Item count to order.
   *
   * @return \ConnectId\Api\DataModel\OrderLine
   *   Order line object.
   */
  protected function getOrderLineForCoupon(CouponType $coupon, int $quantity = 1): OrderLine {
    $line_item = new OrderLine();
    $line_item->withQuantity($quantity)
      ->withProductSpecType('coupon')
      ->withProductSpecCode($coupon->getCouponCode())
      ->withProductSpecNo($coupon->getCouponNumber())
      ->withUnitPrice($coupon->getCouponPrice());

    return $line_item;
  }

  /**
   * Loads the list of available products for this form.
   *
   * @return \ConnectId\Api\DataModel\ProductTypeList
   *   List of products.
   */
  protected function getAvailableProducts(): ProductTypeList {
    return $this->connectIdClientApi->getProducts();
  }

  /**
   * Loads a list of available coupons based on a product.
   *
   * @param string $product_name
   *   The product name to load the relative coupons.
   *
   * @return \ConnectId\Api\DataModel\CouponTypeList
   *   List of coupons.
   */
  protected function getAvailableCoupons(string $product_name): CouponTypeList {
    return $this->connectIdClientApi->getProductCoupons($product_name);
  }

}
