<?php

namespace Drupal\connectid_login\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a ConnectID API Access form.
 */
class DefaultOrderForm extends BaseOrderForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'connectid_default_order';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['product'] = $this->getProductSelectionElement($form_state);

    $form['billing_address'] = $this->getAddressFieldset($form_state, 'Address');

    // TODO: Configure those?
    $required_elements = ['street', 'postalPlace', 'phone', 'email'];
    foreach ($required_elements as $item_key) {
      $form['billing_address'][$item_key]['#required'] = TRUE;
    }

    $form['separate_shipping_address'] = [
      '#type'  => 'checkbox',
      '#title' => t('Different shipping address.'),
      '#defauly_value' => $form_state->getValue('separate_shipping_address'),
    ];
    $form['shipping_address'] = $this->getAddressFieldset($form_state, 'Shipping address');
    $form['shipping_address']['#states'] = [
      'visible' => [
        ':input[name="separate_shipping_address"]' => ['checked' => TRUE],
      ],
    ];

    // Get the payment method form element.
    $form['paymentMethod'] = $this->getPaymentMethodElement($form_state);


    $form['actions'] = [
      '#type'   => 'actions',
      '#weight' => 50,
    ];
    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit order'),
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // TODO
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order = $this->createOrderFromSubmission($form_state);
    $order = $this->submitOrder($form_state, $order);
    $this->setOrderFulfillRedirect($form_state, $order);
  }
}
