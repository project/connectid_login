<?php

namespace Drupal\connectid_login;

use ConnectId\Api\DataModel\Order;

interface OrdersHelperInterface {

  /**
   * Stores an order in a temporary location for later use.
   *
   * @param \ConnectId\Api\DataModel\Order $order
   *   The order to store.
   *
   * @return string
   *   The UUID of the stored order.
   */
  public function temporaryStoreOrder(Order $order): string;

  /**
   * Retrieves a stored order from the temporary location.
   *
   * @param string $order_uuid
   *   The UUID of the order.
   *
   * @return \ConnectId\Api\DataModel\Order|null
   *   The order object when found, NULL otherwise.
   */
  public function getStoredOrder(string $order_uuid): ?Order;

  /**
   * Deletes a stored order from the temporary location.
   *
   * @param string $order_uuid
   *   The UUId of the order to delete.
   */
  public function removeStoredOrder(string $order_uuid): void;
}
