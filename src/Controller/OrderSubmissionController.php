<?php

namespace Drupal\connectid_login\Controller;

use ConnectId\Api\DataModel\Order;
use ConnectId\OAuth2\Client\Provider\Exception\InvalidAccessTokenException;
use Drupal\connectid\UserApiServiceInterface;
use Drupal\connectid_login\OrdersHelperService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\Core\Url;
use Drupal\openid_connect\OpenIDConnectSessionInterface;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for ConnectID Login routes.
 */
class OrderSubmissionController extends ControllerBase {

  /**
   * The current OpenId Connect session for ConnectId Auth.
   *
   * @var \Drupal\openid_connect\OpenIDConnectSessionInterface
   */
  protected OpenIDConnectSessionInterface $oidcSession;

  /**
   * ConnectID User/Logged-in API
   * @var \Drupal\connectid\UserApiServiceInterface
   */
  protected UserApiServiceInterface $connectIdUserApi;

  /**
   * Orders helper service.
   *
   * @var \Drupal\connectid_login\OrdersHelperService
   */
  protected OrdersHelperService $ordersHelper;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $temp_store_factory
   *   The shared tempstore factory.
   */
  public function __construct(
    UserApiServiceInterface $user_api_service,
    OpenIDConnectSessionInterface $oidc_session,
    OrdersHelperService $orders_helper_service
  ) {
    $this->oidcSession = $oidc_session;
    $this->connectIdUserApi = $user_api_service;
    $this->ordersHelper = $orders_helper_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('connectid.user_api'),
      $container->get('openid_connect.session'),
      $container->get('connectid_login.orders_helper')
    );
  }

  /**
   * Submits the order and redirects the user
   */
  public function submitOrder(Request $request, string $order_uuid): Response {
    $order = $this->ordersHelper->getStoredOrder($order_uuid);
    // If the order cannot be loaded drop the ball.
    if ($order === NULL) {
      throw new NotFoundHttpException();
    }

    $token = $this->oidcSession->retrieveAccessToken();
    // If the user has no access token we need to re-authenticate with Conenct ID.
    if ($token === NULL || $this->currentUser()->isAnonymous()) {
      // Ensure user is not currently logged in and redirect the user.
      user_logout();
      $submit_order_url = Url::createFromRequest($request);
      \Drupal::service('page_cache_kill_switch')->trigger();
      return $this->redirect('openid_connect.login', [], ['query' => ['destination' => $submit_order_url->toString()]]);
    }

    try {
      $order = $this->connectIdUserApi->registerOrder(
        new AccessToken(['access_token' => $token]),
        $order
      );
    }
    catch (InvalidAccessTokenException $exception) {
      // Expired token, but we should have just been logged in.
      watchdog_exception('connectid_login', $exception);
      throw new AccessDeniedHttpException();
    }

    // Magic data set in the order process ot handle the redirect.
    if ($order->hasExtra('sourceNodeId')) {
      $return_url = Url::fromRoute('entity.node.canonical', ['node' => $order->getExtra('sourceNodeId')]);
    }
    else {
      // Default to front if we're missing this info.
      $return_url = Url::fromRoute('<front>');
    }
    $return_url = $return_url->setAbsolute()->toString(TRUE)->getGeneratedUrl();

    // If the order failed we send the user back where he came from, with a non-helpful :sad: error message.
    if (!$order->getOrderId()) {
      $this->messenger()->addError(
        $this->t(
          'We could not submit your order, please try again. If the problem persists, please contact support. Error reference: @uuid',
          ['@uuid' => $order_uuid]
        )
      );
      return new RedirectResponse($return_url);
    }

    \Drupal::service('page_cache_kill_switch')->trigger();
    $response = new RedirectResponse(
      $this->connectIdUserApi->getOrderFulfillmentUrl($order, $return_url, $return_url)->getUri()
    );
    // Redirect the user to the payment page on ConnectID.
    $this->ordersHelper->removeStoredOrder($order_uuid);
    return TrustedRedirectResponse::createFromRedirectResponse($response);
  }
}
