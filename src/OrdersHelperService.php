<?php

namespace Drupal\connectid_login;

use ConnectId\Api\DataModel\Order;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\TempStore\SharedTempStore;
use Drupal\Core\TempStore\SharedTempStoreFactory;

/**
 * Helper class to manage orders.
 */
class OrdersHelperService implements OrdersHelperInterface {

  /**
   * Storage factory service.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected SharedTempStoreFactory $tempStore;

  /**
   * Uuid Service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuidService;

  /**
   * Constructor for dependency injection.
   */
  public function __construct(
    SharedTempStoreFactory $temp_store_factory,
    UuidInterface $uuid_service
  ) {
    $this->tempStore = $temp_store_factory;
    $this->uuidService = $uuid_service;
  }

  /**
   * {@inheritDoc}
   */
  public function temporaryStoreOrder(Order $order): string {
    $order_uuid = $this->getRandomIdentifier();
    $this->storage()->set($order_uuid, $order);
    return $order_uuid;
  }

  /**
   * {@inheritDoc}
   */
  public function getStoredOrder(string $order_uuid): ?Order {
    return $this->storage()->get($order_uuid) ?: NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function removeStoredOrder(string $order_uuid): void {
    $this->storage()->delete($order_uuid);
  }

  /**
   * Returns the shared temp store where orders are stored.
   *
   * @return \Drupal\Core\TempStore\SharedTempStore
   *   The shared temp store where orders are stored.
   */
  protected function storage(): SharedTempStore {
    return $this->tempStore->get('connectid_login');
  }

  /**
   * Generates a random identifier.
   *
   * @return string
   *   The generated random identifier.
   */
  protected function getRandomIdentifier(): string {
    return $this->uuidService->generate();
  }
}
