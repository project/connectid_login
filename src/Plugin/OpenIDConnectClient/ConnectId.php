<?php

namespace Drupal\connectid_login\Plugin\OpenIDConnectClient;

use ConnectId\Api\DataModel\ConnectIdProfile;
use ConnectId\OAuth2\Client\Provider\Endpoints;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openid_connect\Annotation\OpenIDConnectClient;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements an OAuth Client plugin for ConnectID by Mediaconnect
 *
 * @OpenIDConnectClient(
 *   id = "connectid",
 *   label = @Translation("ConnectID by MediaConnect")
 * )
 */
class ConnectId extends OpenIDConnectClientBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['testing'] = [
      '#title' => $this->t('Use testing endpoint'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['testing'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getEndpoints(): array {
    $testing = $this->configuration['testing'] ?? FALSE;
    return [
      'authorization' => Endpoints::getBaseAuthorizationUrl($testing),
      'token'         => Endpoints::getBaseAccessTokenUrl($testing),
      'userinfo'      => Endpoints::getResourceOwnerDetailsUrl($testing),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function authorize($scope = 'openid email', array $additional_params = []): Response {
    return parent::authorize('', $additional_params);
  }

  /**
   * {@inheritDoc}
   */
  public function retrieveUserInfo($access_token): ?array {
    $data = parent::retrieveUserInfo($access_token);
    if (empty($data)) {
      return $data;
    }
    $profile = ConnectIdProfile::createFromApiResponse($data);
    return [
      'sub' => $profile->getUniqueId(),
      'email' => $profile->getEmailDefault(),
      // Include the RAW response data/
      '_raw' => $data,
      '_object' => $profile,
    ];
  }
}
