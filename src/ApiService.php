<?php

namespace Drupal\connectid_login;

use Drupal\connectid\UserApiServiceInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;

class ApiService implements ApiServiceInterface {

  /**
   * Logging service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Current logged in user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The ConnectID API service.
   *
   * @var \Drupal\connectid\UserApiServiceInterface
   */
  protected UserApiServiceInterface $connectIdUserApi;

  /**
   * Simple constructor.
   */
  public function __construct(
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    UserApiServiceInterface $connectid_api
  ) {
    $this->logger = $logger;
    $this->currentUser = $current_user;
    $this->connectIdUserApi = $connectid_api;
  }
}
